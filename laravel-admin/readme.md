https://gitee.com/laravel-admin/laraveladmin


https://www.laravel-admin.org/docs/zh/1.x/installation

https://www.laravel-admin.org/


https://laravel-admin.org/docs/zh/2.x


在十分钟内构建一个功能齐全的管理后台

composer require encore/laravel-admin




## 【Laravel 扩展推荐】
 
> Laravel Gii 为中小型项目快速创建管理后台，提供了一种新的可能。使用的过程中，你会发现很轻量，自由度很高。
> 
> 特别是熟悉iView的开发者，在生成的页面上，可以根据自己的需求，自定义页面，将默认Input修改为其他功能丰富的iView组件
> 
> [扩展官网地址：laravel-gii](https://sunshinev.github.io/laravel-gii-home)
> 
> [Github:https://github.com/sunshinev/laravel-gii](https://github.com/sunshinev/laravel-gii)
 
 
![](https://github.com/sunshinev/remote_pics/raw/master/laravel-gii/gii-model-preview.gif)