
# laravel

我一直都不喜欢大框架比如symfony之类的，因为大框架会加载 很多不必须的类和组件，但是大框架也有自身的好处。代码更规范些，使用了工业设计模式。而小框架使用设计模式则少些，但是速度更快些。其实很纠结。

https://laravel-admin.org/

[voyager](https://github.com/the-control-group/voyager) Voyager - The Missing Laravel Admin https://voyager.devdojo.com

[Laravel 5.1 LTS 速查表](https://cs.laravel-china.org)

[2016 版 Laravel 系列入门教程](http://www.cnblogs.com/grimm/category/767554.html)

https://code.tutsplus.com/tutorials/25-laravel-tips-and-tricks--pre-92818


**[ LaraCMS 开发手册 ](https://www.kancloud.cn/wanglelecc/laracms/840008)**

# 学习网站

http://www.golaravel.com

http://www.laravelacademy.org

[中文文档](https://docs.golaravel.com/docs/5.0/installation/)

[video](https://ninghao.net/video/1837)


# Installing Laravel

第一步：composer global require "laravel/installer"

第二步：laravel new blog

第三步：d:\blog\public>php -S localhost:8080 -t .


# 单元测试

[[译] Laravel 5 之美 - 单元测试](https://segmentfault.com/a/1190000011296844) 

# 数据库迁移

[8.4. 数据库迁移](https://laravel-china.org/index.php/docs/laravel/5.6/migrations)

[【 Laravel 5.6 文档 】 数据库操作 —— 迁移](http://laravelacademy.org/post/8845.html)

# blog

https://www.cnblogs.com/helloJiu/category/924892.html

[轻松学会Laravel-基础篇](http://www.imooc.com/learn/697)

[基于Laravel开发博客应用系列 —— 十分钟搭建博客系统](http://laravelacademy.org/post/2265.html)

[laravel 文档](https://www.yiibai.com/laravel/insert_records.html)

[laravel-learning](https://github.com/RunnerLee/laravel-learning) laravel-learning

[laravel-admin](https://github.com/z-song/laravel-admin) Build a full-featured administrative interface in ten minutes http://laravel-admin.org


# 进销存系统

[Discover](https://github.com/youyingxiang/Discover) Discover 是一个基于 Dcat-admin 开发的进销存系统


# API 接口框架

[xApiManager](https://gitee.com/duolatech/xapimanager) XAPI MANAGER -专业实用的开源接口管理平台，为程序开发者提供一个灵活，方便，快捷的API管理工具，让API管理变的更加清晰、明朗。如果你觉得xApi对你有用的话，别忘了给我们点个赞哦^_^ ！ http://xapi.smaty.net/

[ApiAdmin](https://gitee.com/apiadmin/ApiAdmin) 基于ThinkPHP V5.0.16开发的面向API的后台管理系统！ http://www.apiadmin.org/

[Xblog](https://github.com/lufficc/Xblog) A powerful and responsive blog system powered by laravel 5.5. https://lufficc.github.io/Xblog/

[laravel5-example](https://github.com/bestmomo/laravel5-example) Simple laravel5 example for tutorial

[api-watcher](https://github.com/RunnerLee/api-watcher) API monitor

# shop项目

[laravel-shop](https://github.com/summerblue/laravel-shop)

# bbs

[g9zz-bbs](https://github.com/g9zz/g9zz-bbs) Leverage the front and rear end of the interface developed by Laravel! A simple forum system https://www.g9zz.com/


# blog项目

[laravel-5-myblog](https://github.com/xzghua/laravel-5-myblog) The php blog writed by laravel5.1 http://www.iphpt.com

[meedu](https://github.com/Qsnh/meedu) 基于Laravel开发的在线点播系统。

[AutoBuy](https://github.com/Qsnh/AutoBuy)自动售货系统

[moell-blog](https://github.com/moell-peng/moell-blog) 基于 Laravel 开发，支持 Markdown 语法的开源博客 http://moell.cn

[laravel-bjyblog](https://gitee.com/baijunyao/laravel-bjyblog) 基于laravel开发的的个人博客系统laravel-bjyblog https://baijunyao.com/

[myPersimmon](https://github.com/cong5/myPersimmon) 基于Laravel 5.4 的开发的博客系统，代号：myPersimmon https://cong5.net/

[iDashboard](https://gitee.com/iwl/iDashboard) 基于Laravel5.3的简单后台权限管理系统，基本的后台权限完成，后续会根据计划来升级后台功能~ http://iwanli.me

[new_blog](https://gitee.com/kesixin/new_blog)

简介 一个基于Laravel开发，支持markdown语法开源的简易博客。 
功能介绍 markdown文章编辑器 文章发布 评论功能（3.25添加评论功能，评论回复邮件提醒功能） 
浏览数统计 文章分类 文章标签 导航栏自定义 文章评论 关键词 搜索功能 系统基本设置 友情链接 文件上传管理
依赖开源程序 LAMP Laravel AdminLTE Bootstrap Editor.md andersao/l5-repository etrepat/baum
