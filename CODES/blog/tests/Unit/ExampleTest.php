<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
    public function testBasicExample()
    {
        $this->visit('/')->see('Laravel学院');
    }
    /**
     * Test accessor method
     *
     * @return void
     */
    public function testAccessorTest()
    {
        $db_post = DB::select('select * from posts where id = 1');
        $db_post_title = ucfirst($db_post[0]->title);

        $model_post = Post::find(1);
        $model_post_title = $model_post->title;

        $this->assertEquals($db_post_title, $model_post_title);
    }
}
