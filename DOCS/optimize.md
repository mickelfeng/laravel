
[FROM](https://segmentfault.com/a/1190000007894118)

[在PHP7下安装Stone大幅度提升Laravel框架性能](https://segmentfault.com/a/1190000005826835)

[记一次 Laravel 应用性能调优经历](https://segmentfault.com/a/1190000011569012)

[laravel-s](https://github.com/hhxsv5/laravel-s)(🚀 LaravelS: Speed up Laravel/Lumen by Swoole, 'S' means Swoole, Speed, High performance.)

### 1 Laravel的速度瓶颈在哪？


```
针对fpm下的laravel/lumen调优基本是无用功，常驻内存才是调优的重点。
已经有造好的轮子，LaravelS https://github.com/hhxsv5/laravel-s . 通过Swoole来加速Laravel/Lumen，

常驻内存，内置HTTP服务器，平滑Reload，与Nginx配合搭建高可用分布式服务器群，开箱即用。
```


#### 1.1 已有的一些优化方法

1.1.1 laravel官方提供了一些优化laravel的优化方法


```
php artisan optimize
php artisan config:cache
php artisan route:cache
```

* * *

1.1.2 使用opcache加速，PHP是个解释型语言执行的时候先得把程序读进来，由Zend引擎编译成opcode。最后Zend虚拟机顺次执行这些opcode完成操作。opcache起到的作用就是缓存opcode，从而减少编译的时间，减少CPU密集。

* * *

1.1.3 使用PHP7.1，不要问我为什么

#### 1.2 磁盘IO上的瓶颈

Laravel本身启动需要的文件就很多，外加其出了名的生态环境好，开发中我们会很多很多现有的轮子，使得一次启动的磁盘IO特别高（就是要加载很多文件嘛），虽然官方的php artisan optimize方法优化了文件的加载，但并没有实际解决IO上的问题。
知道了问题那就很容易解决了，只要不要每次启动都重新加载就好了，下面轮到Swoole上场啦。

* * *

### 2 Swoole

Swoole是一个PHP扩展，使得PHP使用异步的方式执行，就像node一样，而且还能使用socket，为PHP提供了一系列异步IO、事件驱动、并行数据结构功能。具体的安装方法这就不说了，自己谷歌吧。

* * *

### 3 现有的轮子

搜搜github上已有的swoole启动laravel的轮子，找了三个轮子

[scil/LaravelFly](https://github.com/scil/LaravelFly)
[chongyi/swoole-laravel-framework](https://github.com/chongyi/swoole-laravel-framework)
[garveen/laravoole](https://github.com/garveen/laravoole)

用了LaravelFly，听名字感觉感觉挺酷，结果不如人意，实在不喜欢它那种强硬的启动方式。跟Laravel的风格-'优雅' 很不搭。于是又想自己写，结果写到一半发现laravoole这个项目有更新，然后启动方式（使用artisan命令，没更新前是用的bash脚本启动），代码风格都很酷，这不就是我想做的东西嘛！

* * *

chongyi/swoole-laravel-framework这个轮子是我在写轮子的时候，作者在微信群里分享的，有兴趣的朋友可以试试，我还没试过。

* * *

### 4 LARAVOOLE中的几个注意点

可以看看[作者的文档](https://github.com/garveen/laravoole),我就只总结下我在用的过程中遇到的几个点
1 你应该不再使用以下的超全局变量，因为它们是WEB服务器创建的，而一个非热启动的项目使用他们可能会造成变量污染，你可以从Laravel的Request类中拿到你要的数据。

```
$GLOBALS
$_SERVER
$_REQUEST
$_POST
$_GET
$_FILES
$_ENV
$_COOKIE
$_SESSION
```

2 因为我要开发微信相关的，所以使用了[EASYWECHAT](https://github.com/overtrue/wechat)这个包，但是这个包的oauth方法使用的是原生的SESSION，所以这边也要改成redis等其它方式去存储session。具体代码如下。

```
 //在你的控制器或者中间件中
  public function handle(Request $request, Closure $next)
  //省略代码
  $redirect = config('app.url') . $request->getRequestUri();//这个地址要求带着token

  $options = [
                'app_id' => config('app.appid'),
                'secret' => config('app.secret'),
                'oauth' => [
                    'scopes' => ['snsapi_userinfo'],
                    'callback' => $redirect,
                ],
             ];

   $app = new Application($options);
   //使用laravel session替代原生session
   $app->oauth->setRequest($request);
   //省略下面代码
  }
```

3 不支持热启动了，所以每次更新代码后都需要重新启动Laravoole进程。

```
$ php artisan laravoole restart
```

如需要支持热启动，请自行谷歌 swoole + inotify，大概原理就是用inotify监控文件变更，如果更新了重启swoole,如果正式环境中还可以自己写个部署脚本，git pull后重启服务等，方法很多不一一列举。

### 5 愉快的准备测试啦

测试机子：
阿里云
centos6.5
双核
4G
无视带宽影响，向本机请求，测试结果如下，测了几次，平均在700RPS左右。原先的只有20多RPS。

![clipboard.png](images/1778796736-585bf4dfbd868_articlex.png)

![clipboard.png](images/966717936-585bf1fd114a8_articlex.png)
