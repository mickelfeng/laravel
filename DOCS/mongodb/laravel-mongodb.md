https://github.com/jenssegers/laravel-mongodb

Step1. 根据laravel对应的版本，安装相应的laravel扩展

[https://github.com/jenssegers/laravel-mongodb](https://github.com/jenssegers/laravel-mongodb)


Step2. 在config/app.php中注册provider

    'providers' => [
        ...,
        Jenssegers\Mongodb\MongodbServiceProvider::class,
        ...
    ]
    
Step3. 在config/database.php配置mongodb数据库连接信息

    'connections' => [
        ...,
        'mongodb' => [
                'driver' => 'mongodb',
                'host' => env('DB_HOST', '127.0.0.1'),
                'port' => env('DB_PORT', '27017'),
                'database' => env('DB_DATABASE', 'cpl'),
                'username' => env('DB_USERNAME', 'admin'),
                'password' => env('DB_PASSWORD', 'admin123'),
                'options' => [
                    // here you can pass more settings to the Mongo Driver Manager
                    // see https://www.php.net/manual/en/mongodb-driver-manager.construct.php under "Uri Options" for a list of complete parameters that you can use
                    'database' => env('DB_AUTHENTICATION_DATABASE', 'admin'), // required with Mongo 3+
                    'authSource' => 'admin',
            ],
        ],
        ...,
    ]

Step4. 替换App/User.php中的依赖

    // 将use Illuminate\Foundation\Auth\User as Authenticatable;
    // 替换为use Jenssegers\Mongodb\Auth\User as Authenticatable;


Step5. 为App/User.php添加必要的属性（支持软删除--可选）

    use Jenssegers\Mongodb\Eloquent\SoftDeletes;
    
    class User extends Authenticatable {
        ...
        use SofeDeletes;
        /**
        * mongodb collection 名字
        */protected$collection = 'users';
     
        /**
        * 用于支持软删除
        */protected$dates = ['deleted_at'];
        ...
    }


参考链接：[https://github.com/jenssegers/laravel-mongodb#authentication](https://github.com/jenssegers/laravel-mongodb#authentication)

[http://programming-tips.in/configure-mongodb-in-laravel/](http://programming-tips.in/configure-mongodb-in-laravel/)

[https://stackoverflow.com/questions/37323300/laravel-authentication-and-jenssegers-mongodb-not-working](https://stackoverflow.com/questions/37323300/laravel-authentication-and-jenssegers-mongodb-not-working)

 

打完收工。