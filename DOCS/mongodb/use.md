# [在 Laravel 中使用 MongoDB ](https://learnku.com/articles/2560/using-mongodb-in-laravel)

- **环境准备**

1. [安装 MongoDB](https://learnku.com/laravel/t/2558)
2. [安装 PHP-MongoDB 扩展](https://learnku.com/laravel/t/2559)

## 安装 Laravel-MongoDB

- **推荐组件**

    composer require jenssegers/mongodb

- **注册服务**

    Jenssegers\Mongodb\MongodbServiceProvider::class

- **添加 Facades**

    'Mongo'=>Jenssegers\Mongodb\MongodbServiceProvider::class

- **修改数据库配置文件 config/database.php 中**
```
添加 MongoDB 的数据库的信息:
'mongodb' => [    
        'driver'   => 'mongodb',    
        'host'     => 'localhost',    
        'port'     => 27017,    
        'database' => 'mydb',    
        'username' => '',    
        'password' => '',
],

'default' => env('DB_CONNECTION', 'mysql'),

改成:

'default' => env('DB_CONNECTION', 'mongodb'),
```

## 使用篇

### **`查询构造器`**
```
// 建立一个 UserController.php 控制器
php artisan make:controller UserController
参考代码:
use DB;   //引用数据库

class MongoController extends Controller{
    pubulic function index(){
        DB::collection('users')               //选择使用users集合
              ->insert([                          //插入数据
                      'name'  =>  'tom', 
                      'age'     =>   18
                  ]);
    }

    $res = DB::collection('users')->all();  //查询所有数据
    dd($res);                                            //打印数据
}
```
- **设置一个访问路由，然后测试**

> 如果你没有修改默认的数据库配置 (默认还是 MySQL), 那么你在使用 MongoDB 的时候就要指定使用 MongoDB 了

- **例如:**
```
use DB;   //引用数据库

class MongoController extends Controller{
    pubulic function index(){
        DB::connection('mongodb')       //选择使用mongodb
              ->collection('users')           //选择使用users集合
              ->insert([                          //插入数据
                      'name'  =>  'tom', 
                      'age'     =>   18
                  ]);
    }

    $res = DB::connection('mongodb')->collection('users')->all();   //查询所有数据
    dd($res);                                            //打印数据
}
```
> 有关查询构造器的使用和 MySQLi 的方式是一样的，参照 `Laravel 文档查询构造器`

### **`Eloquent 模型`**

- 在 config/app.php 配置文件中配置 MongoDB 的 Eloquent 类的别名

    'Moloquent'=>'Jenssegers\Mongodb\Eloquent\Model'

- 新建一个 User.php 的 Model 类

    php artisan make:model UserCopy

- 参考代码
```
<?php
    namespace App;
    use Moloquent;
    use DB;

    class Users extends Moloquent{    
        protected $connection = 'mongodb';  //库名    
        protected $collection = 'users';     //文档名    
        protected $primaryKey = '_id';    //设置id    
        protected $fillable = ['id', 'name', 'phone'];  //设置字段白名单
    }
```
- 在 UserController.php 控制器中这样使用

```
<?phpnamespace 
    App\Http\Controllers;
    use App\Users;    //引入Users模型

    class MongoController extends Controller{
        public function index(){
        Users::create([                      //插入数据
            'id'     =>1,
            'name'   =>'tom',
            'phone'  =>110]);
        }

        dd(Users::all());          //查询并打印数据
```