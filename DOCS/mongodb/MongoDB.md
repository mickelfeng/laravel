
[PHP Laravel配置使用MongoDB](https://www.jianshu.com/p/5f31d24899fa)

### 一、环境介绍

1. 操作系统：macOS Catalina
2. PHP：7.4
3. Laravel： 7.24
4. MongoDB：4.4.0

### 二、安装MongoDB

使用brew进行安装

    brew tap mongodb/brew
    brew install mongodb-community
    //如果想安装指定版本的mongodb
    brew install mongodb-community@4.4

如果没有安装brew建议自行安装（[brew官网](https://links.jianshu.com/go?to=https%3A%2F%2Fbrew.sh%2F),[mac安装homebrew失败怎么办？](https://links.jianshu.com/go?to=https%3A%2F%2Fwww.zhihu.com%2Fquestion%2F35928898)）

检验MongoDB是否安装成功

    mongo --version
    

如果输出mongodb的相关信息则代表安装成功。

### 三、php mongodb driver安装

    sudo pecl install mongodb
    

查看php.ini中是否自动添加了*extension=mongodb.so*，若没有则自己添加（这是在PHP中启用mongodb扩展，pecl是php extension comminity library，PHP安装时应该就会安装的）

**重启PHP服务！！！**

1. 先查看你的php服务叫什么

    brew services list
    

1. 重启服务（php更换为你的php服务）

    brew services restart php
    

### 四、安装配置jenssegers/laravel-mongodb

1. 进入项目根目录，执行下面的指令

    composer require jenssegers/mongodb 3.7.x
    

在[https://github.com/jenssegers/laravel-mongodb](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fjenssegers%2Flaravel-mongodb)查找自己laravel版本对应的版本

1. 在config/app.php中添加服务

    Jenssegers\Mongodb\MongodbServiceProvider::class,



```
 代码如下 ：

'providers' => [
....
 /**
         * Mongodb
         */
        Jenssegers\Mongodb\MongodbServiceProvider::class,
],


'aliases' => [
....
'Mongo'     => Jenssegers\Mongodb\MongodbServiceProvider::class,
]
```

1. 在config/database.php中配置mongodb连接

```
'mongodb' => [
    'driver' => 'mongodb',
    'host' => env('MongoDB_HOST', '127.0.0.1'),
    'port' => env('MongoDB_PORT', 27017),
    'database' => env('MongoDB_DATABASE', 'phpmongo'),
    'username' => env('MongoDB_USERNAME', ''),
    'password' => env('MongoDB_PASSWORD', ''),
    'options' => [
        // here you can pass more settings to the Mongo Driver Manager
        // see https://www.php.net/manual/en/mongodb-driver-manager.construct.php under "Uri Options" for a list of complete parameters that you can use

        'database' => env('DB_AUTHENTICATION_DATABASE', 'admin'), // required with Mongo 3+
    ],
],
```
在.env文件中配置变量

    MongoDB_HOST=127.0.0.1//mongodb的ip
    MongoDB_PORT=27017//mongodb的port
    MongoDB_DATABASE=phpmongo   //使用的database名称

至此mongodb的安装与配置已全部完成，如果你只使用mongodb，可以将database.php的`'default' => env('DB_CONNECTION', 'mysql')`更改为`'default' => env('DB_CONNECTION', 'mongodb')`。

### 五、mongodb简单的操作

1. 创建模型

模型可以参考下面的代码
```
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Blog extends Model
{
    //指定数据库集合
    protected $collection = 'blog';

    //指定数据库连接
    protected $connection = 'mongodb';

    //启用softdelete
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    //数据库不接受更改的内容，除此之外全部接受
    protected $guarded = ['id'];

    //数据库接受更改的内容
    //protected $fillable = ['title'];

    //属性默认值
    protected $attributes = [
        'isdelete' => false,
    ];
}
```
其中`$connect`指定database的连接（这在同时使用mysql与mongodb时非常有用）;

`$collection`指定使用的mongodb集合；softdeletes启用之后，deleted将不会再删除数据，同时会集合的所有文档（数据）都会自带`create_at`和`update_ate`字段，`create_at`和`update_at`将会在文档插入时创建，在文档更新时`update_at`也会随之更新。

2. 插入
```
public function addBlog(Request $request) {
    $title = $request->get('title');
    //DB::connection('mongodb')->collection('blog')->insert(['title' => $title])
    Blog::create(['title' => $title]);
}
```
3. 更新
```
public function deleteBlog(Request $request) {
    $id = $request->get('id');
    Blog::where('_id', $id)->update(['isdelete' => true]);
}
```
4. 更新
```
public function getBlogList() {
    return json_encode([
        'blogList' =>Blog::where('isdelete', false)->get()
    ], JSON_UNESCAPED_UNICODE);
}
```
具体的操作可以参考[Laravel Eloquent ORM](https://links.jianshu.com/go?to=https%3A%2F%2Flearnku.com%2Fdocs%2Flaravel%2F5.8%2Feloquent%2F3931)

参考文章：[Install MongoDB Community Edition on macOS](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.mongodb.com%2Fmanual%2Ftutorial%2Finstall-mongodb-on-os-x%2F),[MongoDB PHP Driver](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.mongodb.com%2Fdrivers%2Fphp),[jenssegers/laravel-mongodb](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Fjenssegers%2Flaravel-mongodb%23installation),