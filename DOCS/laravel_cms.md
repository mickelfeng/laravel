
[网站](https://laravelacademy.org/project/website/page/4)

[laraadmin](https://github.com/dwijitsolutions/laraadmin)

[yicms](https://github.com/JeffreyBool/yicms) 基于laravel5.5 开发的Yicms系统的基础架构

[Borgert](https://github.com/odirleiborgert/borgert-cms) Borgert 是一款基于 Laravel 5.4 框架开发的开源 CMS 系统，包含了诸多模块，例如博客、页面、商品、邮箱、图片库、用户等，你可以将其作为项目原型快速开始业务迭代开发。

[f-admin](https://gitee.com/fzsfzs/f-admin) f-admin是一套基于Laravel框架开发的基础权限后台系统http://f-admin.fang99.cc

[TypiCMS系统](https://github.com/TypiCMS/Base)多语言和模块化的CMS Laravel 5.2框架

[BootstrapCMS系统](https://github.com/BootstrapCMS/CMS)

[Laravel-Administrator系统](https://github.com/FrozenNode/Laravel-Administrator)

[octobercms 系统](https://github.com/octobercms/october)

[LavaLite cms系统](https://github.com/LavaLite/cms)

[芽丝内容管理框架(YASCMF)](https://github.com/douyasi/yascmf)

[laracms](https://github.com/wanglelecc/laracms) LaraCMS 是在学习 laravel （ web 开发实战进阶 + 实战构架 API 服务器） 过程中产生的一个业余作品，试图通过简单的方式，快速构建一套基本的企业站同时保留很灵活的扩展能力和优雅的代码方式，当然这些都得益Laravel的优秀设计。同时LaraCMS 也是一个学习Laravel 不错的参考示例。 https://www.laracms.cn/

[KodiCMS 系统](https://github.com/KodiCMS/kodicms-laravel)CMS 以 Laravel 5.2为核心开发