[laravelcms](https://github.com/webbc/laravelcms) A Laravel CMS


[yascmf](https://github.com/douyasi/yascmf) 已过时，请访问5.2新版仓库 https://github.com/yascmf/base

### 原型项目

*   [Laravel 5 Boilerplate](http://laravelacademy.org/post/1359.html) ——&nbsp;基于当前Laravel最新版本（Laravel 5.1.*）并集成Boilerplate的项目
*   [Laravel 5 Angular Material Starter](http://laravelacademy.org/post/1426.html) ——&nbsp;这是一个Laravel 5.1和AngularJS的原型项目
*   [Someline Starter](http://laravelacademy.org/post/6408.html) —— 基于Laravel 5和Vue.js，用于快速构架RESTful API和Web应用的原型项目

### CMS

*   [Bootstrap CMS](http://laravelacademy.org/post/709.html) ——&nbsp;Laravel 5.1驱动的功能强大的CMS
*   [October](http://laravelacademy.org/post/1132.html) —— 基于Laravel 5，致力于让开发工作变得简单的CMS
*   [PyroCMS](http://laravelacademy.org/post/1391.html) ——&nbsp;MVC架构的PHP内容管理系统，3.0以前基于CodeIgniter，目前基于Laravel 5.1
*   [LavaLite](http://laravelacademy.org/post/1227.html) ——&nbsp;基于Laravel 5.1 &amp; Bootstrap 3的内容管理系统
*   [TypiCMS](http://laravelacademy.org/post/1508.html) ——&nbsp;基于 Laravel 5 构建的、支持多语言的内容管理系统
*   [Laravel and AngularJS CMS](http://laravelacademy.org/post/1972.html) —— 基于Laravel 5.1和AngularJS的CMS
*   [Microweber](http://laravelacademy.org/post/3876.html) —— 基于 Laravel 拖拽式生成 CMS 及在线商店利器
*   [AsgardCMS](http://laravelacademy.org/post/6417.html) —— 基于 Laravel 构建的、支持模块化和多语言的CMS
*   [CoasterCMS](http://laravelacademy.org/post/6547.html) —— 基于Laravel 5.4构建的下一代CMS
*   [BorgertCMS](http://laravelacademy.org/post/7541.html) —— 基于 Laravel 5.4 开发的开源模块化 CMS
*   [WebEdCMS](http://laravelacademy.org/post/7557.html) —— 基于 Laravel 5.4 开发的开源CMS系统（有主题功能）

*   [base](https://github.com/yascmf/base) YASCMF 基础开发版（YASCMF/BASE） http://www.yascmf.com

*   [cms](https://github.com/LavaLite/cms) Multilingual PHP CMS built with Laravel and bootstrap https://lavalite.org



http://laravelacademy.org/project/website/page/2


[基于 Laravel 5.4 开发的简单模块化开源 CMS 系统 —— Borgert](https://github.com/odirleiborgert/borgert-cms)

[又一个基于 Laravel 5.2 开发的后台管理系统](https://github.com/jwwb681232/zhanghaobao)

[一款基于 Laravel 5.1 和 Vue.js 的项目管理系统 —— Ribbbon](https://github.com/canvasowl/ribbbon.git)

[基于 Laravel 开发的自带前后端主题模板的 CMS 系统 —— WebEd](https://github.com/sgsoft-studio/webed)



### CRM

*   [Flarepoint](http://laravelacademy.org/post/5305.html) —— 基于Laravel构建的免费开源CRM平台

### 论坛

*   [Laravel.io](http://laravelacademy.org/post/666.html) —— Laravel开发者社区
*   [Flarum](http://laravelacademy.org/post/1090.html) ——&nbsp;免费的、开源的、专注于简约的论坛系统，esoTalk和FluexBB的联合继承者
*   [PHPHub](http://laravelacademy.org/post/899.html) —— 基于Laravel 4.2，积极向上的 PHP &amp; Laravel 开发者社区
*   [zhihu-app](http://laravelacademy.org/post/9083.html) —— 基于 Laravel 5.3 +Vue 构建的带前后台的、类似知乎的问答社区

### 社交网站

*   [ThinkSNS+](http://laravelacademy.org/post/7288.html) —— 基于 Laravel + Vue.js 开发的全新社交系统
*   [Voten](http://laravelacademy.org/post/7341.html) —— 基于 Laravel + Vue.js &nbsp;实现的实时社交书签系统（Reddit）

### 电商

*   [Antvel](http://laravelacademy.org/post/3793.html) ——&nbsp;基于 Laravel 5.* 开发的开源电子商务项目

### 项目管理

*   [92five app](http://laravelacademy.org/post/1045.html) ——&nbsp;基于 Laravel框架 &amp; Backbone JS 构建，是一个自托管的、基于web的项目管理应用
*   [Scrumwala](http://laravelacademy.org/post/956.html) —— 基于Laravel5，项目管理应用
*   [Ribbbon](http://laravelacademy.org/post/6495.html) —— 基于Laravel 5.1和Vue.js构建的开源项目管理系统

### 博客

*   [Canvas](http://laravelacademy.org/post/5136.html) —— 基于Laravel 5.2 开发的轻量级博客系统
*   [Katana](http://laravelacademy.org/post/4019.html) —— 静态博客/站点生成器（支持Markdown和GitHub Pages）
*   [Laravel 5 Blog](https://github.com/yccphp/laravel-5-blog) —— 基于&nbsp;Laravel 5 开发的博客系统
*   [Vuedo](http://laravelacademy.org/post/5271.html) —— 基于 Laravel 和 Vue.js 构建的博客平台
*   [Wardrobe](http://laravelacademy.org/post/1203.html) ——&nbsp;专注于写作的最小化博客平台
*   [MyPersimmon](http://laravelacademy.org/post/7119.html) —— 基于 Laravel 5.4 开发的博客系统

### 后台模板

*   [Laravel Angular Admin](http://laravelacademy.org/post/4813.html) ——&nbsp;基于 Laravel + Angularjs + Bootstrap + AdminLTE 实现的后台模板

### 其他

*   [Laravel.com](http://laravelacademy.org/post/788.html) —— Laravel官网源码
*   [Laravel Tricks](http://laravelacademy.org/post/681.html) ——&nbsp;Laravel小技巧&amp;小贴士
*   [Invoice Ninja](http://laravelacademy.org/post/1117.html) ——&nbsp;基于Laravel构建的开源发票及时间跟踪应用
*   [Paperwork](http://laravelacademy.org/post/748.html) —— 基于Laravel 4.X的开源的笔记&amp;归档工具
*   [Cachet](http://laravelacademy.org/post/824.html) —— 基于Laravel 5的开源的状态页系统
*   [StyleCI](http://laravelacademy.org/post/872.html) —— 由 PHP CS Fixer开发，提供PHP代码风格持续集成服务
*   [Podcastwala](http://laravelacademy.org/post/1006.html) —— 基于Laravel 5，构建属于你自己的播客网站
*   [Deployer](http://laravelacademy.org/post/1681.html) ——&nbsp;基于Laravel 5.1的、免费的、开源的PHP应用部署工具
*   [RSS Monster](http://laravelacademy.org/post/2555.html) —— 基于 Lumen 开发的 RSS 聚合器和阅读器
*   [Koel](http://laravelacademy.org/post/2624.html) —— 基于 Laravel 5.1 &amp; Vue.js 开发的酷炫音乐流媒体应用
*   [Attendize](http://laravelacademy.org/post/3615.html) ——&nbsp;基于 Laravel 框架开发的开源门票及活动管理应用
*   [Notadd](http://laravelacademy.org/post/7092.html) ——&nbsp;基于 Laravel 的下一代 PHP 开发框架（API+SPA 单页应用 ）
*   [Blockscloud](http://laravelacademy.org/post/7310.html) &nbsp;—— 基于 Laravel + Vue 实现的 Web OS 系统

### 安装教程

*   [在 Windows 上快速安装 Flarum 指南](http://laravelacademy.org/post/1150.html)
*   [在Windows上快速安装配置PyroCMS](http://laravelacademy.org/post/1659.html)