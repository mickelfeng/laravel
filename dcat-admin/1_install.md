**前言：**

是从 [laravel admin](http://laravel-admin.org/) 接触到 Dcat Admin 的，从自己的使用情况来看，[laravel admin](http://laravel-admin.org/) 很多细节的功能还有点差强人意（当然是自己水平不行）。文档方面，对新手还是有些晦涩，所以做了几个项目之后，尤其是对数据进行批量操作，交互操作的时候，还是很难实现常规项目的要求，所以被迫准备转到 Dcat Admin 上来。

最重要的是，官方群里灌水太严重了，有问题也很少有人能主动回答。或者回答的内容，对新手来讲，还是有一定的难度。Dcat admin 的官方群就不错，作者会主动回答问题，群里基本上讨论的都是技术。

**以上只是自己的一些感悟，并不能说 [laravel admin](http://laravel-admin.org/) 不如 Dcat Admin ，Dcat Admin 的作者也是大量借鉴了 [laravel admin](http://laravel-admin.org/) 的方式，所以，[laravel admin](http://laravel-admin.org/) 也是非常不错的一个 laravel 的扩展。**

言归正传

前提条件：
```
PHP >= 7.1
Laravel 5.5.0 ~ 8.*
Fileinfo PHP Extension
```
 
**第一步：安装 laravel 可以根据自己的需要，指定 laravel 的版本。**
```
composer create-project --prefer-dist laravel/laravel {项目名称} "6.0.*"
# 或
composer create-project --prefer-dist laravel/laravel {项目名称} 
# 如果在安装的时候，提示 Your requirements could not be resolved to an installable set of packages.
可以输入如下命令：
composer create-project --prefer-dist laravel/laravel {项目名称} --ignore-platform-reqs
```
**第二步：配置 .env 文件，设置数据库连接设置正确。**
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={根据仔细需要修改}
DB_USERNAME=root
DB_PASSWORD={根据仔细需要修改}
```

**第三步：配置完 .env 文件后，请在 MySQL 里，增加 .env 里 DB_DATABASE 所对应的数据库。**

**第四步：开始安装 Dcat Admin；**
```
cd {项目名称}

composer require dcat/laravel-admin
# 本教程采用的是 2.* 版本 
composer require dcat/laravel-admin:"2.*" -vvv 
# 或者：
composer require dcat/laravel-admin:"2.1.1-beta"
```
**第五步：然后运行下面的命令来发布资源：**

    php artisan admin:publish
    
在该命令会生成配置文件 config/admin.php，可以在里面修改安装的地址、数据库连接、以及表名，建议都是用默认配置不修改。

> 执行这一步命令可能会报以下错误 Specified key was too long ... 767 bytes，如果出现这个报错，请在 app/Providers/AppServiceProvider.php 文件的 boot 方法中加上代码 Schema::defaultStringLength (191);，然后删除掉数据库中的所有数据表，再重新运行一遍 php artisan admin:install 命令即可。

**第六步：开始安装 Dcat Admin。**

   php artisan admin:install

```
D:\T\DcatAdmin>php artisan admin:install
Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.03 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.03 seconds)
Migrating: 2016_01_04_173148_create_admin_tables
Migrated:  2016_01_04_173148_create_admin_tables (0.23 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.01 seconds)
Database seeding completed successfully.
Admin directory was created: \app\Admin
HomeController file was created: \app\Admin/Controllers/HomeController.php
AuthController file was created: \app\Admin/Controllers/AuthController.php
Bootstrap file was created: \app\Admin/bootstrap.php
Routes file was created: \app\Admin/routes.php
Done.

D:\T\DcatAdmin>
```

```
PS D:\T\DcatAdmin\storage> tree
├─cache
├─framework
│  ├─sessions
│  └─views
└─logs
```

**第七步：Dcat Admin 需要配置虚拟域名，并指向 public 目录。** {如果搞不定的可留言}

**第八步：启动服务后，在浏览器打开 http://{你的虚拟域名}/admin，使用用户名 admin 和密码 admin 登陆。**

安装完成之后，Dcat Admin 所有的配置都在 config/admin.php 文件中。

最后编辑于: 2021 年 07 月 23 日




### dcat Admin 安装爬坑记录

```
------------------------------------------------------------------------------------

查看版本
PHP artisan --version

------------------------------------------------------------------------------------

报错 There are no commands defined in the "admin" namespace
执行命令 composer update

------------------------------------------------------------------------------------

报错Fatal error: Allowed memory size of 1610612736 bytes exhausted (tried to allocate 4096 bytes) 
php -d memory_limit=-1 C:\ProgramData\ComposerSetup\bin\composer.phar update
C:\ProgramData\ComposerSetup\bin\composer.phar 是composer的安装路径 使用composer -v 查看
C:\ProgramData\ComposerSetup\bin\composer.phar update 是命令

------------------------------------------------------------------------------------
php artisan admin:install执行报错
报错 SQLSTATE[42000]: Syntax error or access violation: 1071 Specified key was too long; max key length is 1000 bytes (SQL: alter table `users` add unique `users_email_unique`(`email
方法一
在网上查找了下资料，修改 config/database.php 文件，关键字搜索： utf8mb4 替换成 utf8 便可以了。

方法二
此方法原文链接：[](https://news.laravel-china.org/posts/544)https://news.laravel-china.org/posts/544

修改 app/Providers/AppServiceProvider.php 文件：

use Illuminate\Support\Facades\Schema;

public function boot()
{
    Schema::defaultStringLength(191);
}

删除users 在重新执行命令

dcat Admin 文档建议方法：https://learnku.com/docs/dcat-admin/2.x/install/8081
{tip} 执行这一步命令可能会报以下错误 Specified key was too long ... 767 bytes，如果出现这个报错，请在 app/Providers/AppServiceProvider.php 文件的 boot 方法中加上代码 \Schema::defaultStringLength(191);，然后删除掉数据库中的所有数据表，再重新运行一遍 php artisan admin:install 命令即可。

```