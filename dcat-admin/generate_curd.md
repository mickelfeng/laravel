
config setting 不能生成文件

## 快速开始

在日常开发中，我们可以用代码生成器一键生成增删改查页面代码，非常的方便快捷。

下面将会给大家介绍代码生成器的使用方法，以及一个增删改查页面的基本构成。通过学习下面的内容将可以帮助大家快速理解这个系统的基本使用方法。

## 代码生成器

## 创建数据表

安装完 Laravel 之后会内置一个 users 表的 migration 文件 (如果不了解 migration 文件作用，请参考文档数据库迁移)，文件路径为 database/migrations/2014_10_12_000000_create_users_table.php。

然后我们运行以下命令，在 MySQL 中创建这个数据表

## php artisan migrate

运行完之后可以看到数据库中已经多了一个 users 表，结构如下
```
CREATE TABLE `users` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL, 
 `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL, 
 `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, 
 `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
 `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00', 
 PRIMARY KEY (`id`), 
 UNIQUE KEY `users_email_unique` (`email`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
```
## 一键生成增删改查页面

如果你的开发环境不是 windows，请注意要给项目目录设置读写权限，否则可能出现无法生成代码的情况。

1. 首先打开地址 http://你的域名/admin/helpers/scaffold，进入代码生成器页面；

2. 由于前面已经创建好了数据表，所以这里我们可以直接通过页面左上角的第二个下拉选框选择 users 表，选择之后会自动填充字段信息，效果如下

![](https://upload-images.jianshu.io/upload_images/8878143-285f670168a75944.png?imageMogr2/auto-orient/strip|imageView2/2/w/731/format/webp)

3. 修改模型名称为 App\User

4. 由于 migration 文件、数据表、以及模型文件 (使用内置的 App\User 即可) 都已经有了，所以此处我们可以把这三个选项给去掉

5. 填写字段翻译

最后呈现效果如下

![](https://upload-images.jianshu.io/upload_images/8878143-89c4c3915200ff01.png?imageMogr2/auto-orient/strip|imageView2/2/w/735/format/webp)

最后点击创建按钮即可，创建的文件如下

    app/Admin
    ├── Controllers
    │   └── UserController.php  # 控制器
    └── Repositories            # 数据仓库
    │   └── User.php
    resouces/lang/{当前语言}
    └── user.php                # 语言包
    

添加路由配置

打开路由配置文件 app/Admin/routes.php，往里面添加一行：

$router->resource('users', 'UserController');

到此，就可以打开浏览器输入地址 http://你的域名/admin/users 访问刚刚创建完的页面了