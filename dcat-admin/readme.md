[dcat-admin](https://gitee.com/jqhph/dcat-admin) 只需极少的代码即可快速构建出一个功能完善且颜值极高的后台系统，内置丰富的后台常用组件，开箱即用，让开发者告别冗杂的HTML代码。

[Dcat Admin - 一键生成 CURD 代码](https://www.jianshu.com/p/988db6ce08e1)



[Dcat Admin 入门教程（一）安装 Dcat Admin](http://www.andyzu.com/index.php/archives/104/)

[Dcat Admin 入门教程（二）快速使用 Dcat Admin](http://www.andyzu.com/index.php/archives/122/)

[Dcat Admin 入门教程（三）FORM 表单里的 options () 和 default () 方法](http://www.andyzu.com/index.php/archives/154/)

[Dcat Admin 入门教程（四）关于代码生成器的再次说明](http://www.andyzu.com/index.php/archives/171/)

[Dcat Admin 入门教程（五）开发真正的后台管理系统](http://www.andyzu.com/index.php/archives/174/)

[Dcat Admin 入门教程（六）开发真正的后台管理系统](http://www.andyzu.com/index.php/archives/207/)

[Dcat Admin 入门教程（七）开发真正的后台管理系统 之 图片处理](http://www.andyzu.com/index.php/archives/214/)

[Dcat Admin 入门教程（八）开发真正的后台管理系统 之 模型树](http://www.andyzu.com/index.php/archives/221/)




Dcat Admin 关闭代码生成器

```
laravel5\config\admin.php

‘enable’ => true, 改成false即可

    /*
    |--------------------------------------------------------------------------
    | dcat-admin helpers setting.
    |--------------------------------------------------------------------------
    */
    'helpers' => [
        'enable' => true,
    ],
```




主题与颜色

切换主题

Dcat Admin 支持主题切换功能，目前内置了四种主题色：brue-dark、blue、blue-light、green，可通过配置参数 admin.layout.color 进行切换。

打开配置文件 config/admin.php

     'layout' => [
         'color' => 'blue',

         ...
     ],

     ...
